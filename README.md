# Ansible role ``storage/encryption.luks``

## Description
Configure storage partition encryption with the LUKS framework.